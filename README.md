
# clusterctl

A lightweight successor to c3's cexec utility. Dependencies: linux or macOS, python3.9+, ssh

## Usage

```shell
# Print help information
clusterctl -h

# Execute command on each node in named cluster
clusterctl -t cluster_01: "touch foo.txt"

# Specify multiple clusters
clusterctl -t cluster_01: cluster_02: ...

# Specify nodes and ranges
clusterctl -t all:1,2,3 ...
clusterctl -t all:1-3,5 other:4 another: ...
```

### Help Text

```text
usage: clusterctl [-h] [-i] [-a] [--head] [-t TARGET] [-f FILE] [-u USER] [-p] [-] [-l LOG_LEVEL] [-L LOG_FILE] [-v] [-q] command [command ...]

positional arguments:
  command               Command to execute on targets. To avoid quoting, use '--' to specify that you are done supplying named arguments and the rest of the line should be interpreted as the command. Example: `clusterctl -v -ldebug -t all:1,2,3 -- command and args`

optional arguments:
  -h, --help            show this help message and exit
  -i, --interactive     (UNIMPLEMENTED) Ask before each command is executed.
  -a, --all             Ignore target and execute on all nodes in all clusters.
  --head                Execute command on ONLY the head node of each specified cluster.
  -t TARGET, --target TARGET
                        Specify which clusters/nodes to execute against. Defaults to 'all:' or the value of the CLUSTER_DEFAULT environment variable.
  -f FILE, --file FILE  Specify alternative clusters file. Default: /etc/clusterctl/clusters
  -u USER, --user USER  Specify alternative user to authenticate to the cluster as. Defaults to the current user or the value of the CLUSTER_USER environment variable.
  -p, --password        Allow password prompts instead of normal key-only operation.
  -, --pipe             (UNIMPLEMENTED) Enable parseable formatting
  -l LOG_LEVEL, --log-level LOG_LEVEL
                        Set logging level. Options: DEBUG,INFO,WARN[ING],ERROR,CRITICAL,None Default: None
  -L LOG_FILE, --log-file LOG_FILE
                        Use alternative logging location. Default: ./.clusterctl.log or the value of the CLUSTER_LOG environment variable
  -v, --verbose         Enable verbose output. Log messages that would normally be suppressed are printed to stdout.
  -q, --quiet           Enable 'quiet' mode. Node names and command output is not printed.
```

## Config

cluster-utils uses the same config format as its predecessor software, c3.  The configuration file consists of any number of `cluster` definitions.  A cluster definition looks like this:

```text
cluster servers {       # This cluster is named "servers"
    node1               # "node1" is the `head` node (0)
    node2
    node3
    ...
    nodeN
}
```

The following potential config file paths are attempted in order.  Only the first valid config is used.

- The path specified by the "--path" argument OR `./.clusters` if not present
- `~/.config/clusterctl/clusters`
- `/usr/local/etc/clusterctl/clusters`
- `/etc/clusterctl/clusters`
